# MoneyManagerExLib

Python library (Data Access Layer) for reading Money Manager Ex database.

It uses SQLAlchemy to define the data schema for the MMEX database.

Useful for scripts that read/write the data in .mmb files.

Available on PyPi at https://pypi.org/project/moneymanagerexlib/

## GnuCash Import

GnuCash CSV import offers a "Multi-split" mode. This will import multiple splits for one transaction based on the transaction id.

## Examples

To export to a ledger-compatible file, create a script such as

```
from moneymanagerexlib.ledger import LedgerExporter
input = "~/path/to/my/mmex/file.mmb"
output = "mmex_export.ledger"

exporter = LedgerExporter(input)

with open(output, "w") as out_file:
    out_file.write(exporter.export())
```

This will produce "mmex_export.ledger" file with transactions.

The following scripts use "config.py" file, which is just a separate file for defining the variables like the database path and the accounts to be operated on.

```
from moneymanagerexlib import Configuration

cfg.db_path = "~/path/to/my/mmex/file.mmb"
cfg.accounts = ["Cash"]

cfg = Configuration()
```

Once the transactions have been transferred to your main ledger book, they can be deleted from the MMEx database with the following script:

```
'''
Delete transaction with a specified id.
'''
import sys 
from moneymanagerexlib import Commands
from config import cfg


# id is mandatory
if len(sys.argv) < 2:
    print("missing transaction id")
    exit

id = sys.argv[1]

cmd = Commands(cfg.db_path)
message = cmd.delete_transaction(id)

print(message)
```

This script accepts the transaction id and deletes it from the database.
Once the transactions have been transferred to ledger and removed from MMEx database (if that's what you do), the starting account balances should be updated. This can be done with:

```
import sys
from config import cfg
from moneymanagerexlib import Commands

# parameters are mandatory
if len(sys.argv) < 2:
    print("missing parameter balance")
    exit

#account = sys.argv[1]
#balance = sys.argv[2]
balance = sys.argv[1]

cmd = Commands(cfg.db_path)
account = cfg.accounts[0]
cmd.set_account_balance(account, balance)

print(f"updated {account} to {balance}")
```

## Tips

Here are some handy tips for easier conversion process.

You can use Held At field in the account to add the full prefix. The library uses

`p2.account = f"{tx.account_to.HELDAT}:{tx.account_to.ACCOUNTNAME}"`

to construct the account name. Therefore, you can use "Assets:Bank Accounts" as the Held At value, and "My Bank" as the Account name. In the exported file, this would result in "Assets:Bank Accounts:My Bank", which should match your ledger account name.
