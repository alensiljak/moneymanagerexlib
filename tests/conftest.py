""" Test configuration """
import pytest
from pkg_resources import Requirement, resource_filename
#from pricedb import dal
from moneymanagerexlib import dal


# @pytest.fixture(scope="session")
# def db_path():
#     """ Path to the test database """
#     filename = filename = resource_filename(Requirement.parse("PriceDb"), "data/prices-template.db")
#     return filename


# @pytest.fixture(scope="session")
# def csv_path():
#     """ Path to a test CSV file for price import """
#     filename = filename = resource_filename(
#         Requirement.parse("PriceDb"), "data/AUD_2017-11-11_142445.csv")
#     return filename


# @pytest.fixture(scope="session")
# def db_session():
#     """ Test-database connection """
#     my_db = db_path()
#     session = dal.get_session(my_db)

#     return session

@pytest.fixture(scope="session")
def session():
    """ The in-memory database connection """
    my_db = ":memory:"
    session = dal.get_session(my_db)
    return session


@pytest.fixture(scope="session")
def session_file():
    ''' Creates a session that uses a real file '''
    # hard-coded path :S
    db_path = "P:\\My Documents\\finance\\MoneyManagerEx\\dev\\dev-db.mmb"
    session = dal.get_session(db_path)
    return session
    