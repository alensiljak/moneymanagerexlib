'''
Test DAL: session creation, reading & writing data.
'''
from moneymanagerexlib.dal import Account, CheckingAccount


def test_session_creation(session):
    assert session is not None


def test_write_and_read(session):
    write_tx(session)

    txs = session.query(CheckingAccount).all()
    num_rows = len(txs) # .count()
    
    assert txs is not None
    assert num_rows == 1

    tx: CheckingAccount = txs[0]
    assert tx is not None
    assert tx.ACCOUNTID == 1
    assert tx.TRANSAMOUNT == 58.356


def read_one_tx(session):
    tx = session.query(CheckingAccount).first()
    return tx

def write_tx(session):
    tx = CheckingAccount()

    tx.ACCOUNTID = 1
    tx.PAYEEID = 1
    tx.TRANSCODE = 'N'
    #tx.TRANSID
    tx.TRANSAMOUNT = 58.356

    session.add(tx)
    session.commit()

def test_relationships(session_file):
    ''' Test relationships '''
    account: Account = session_file.query(Account).first()
    cur = account.currency
    assert cur is not None
