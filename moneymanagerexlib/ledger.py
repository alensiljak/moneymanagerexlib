'''
Export directly to ledger format.
- For Accounts, using HeldAt + Account Name for output.
'''

class LedgerPosting():
    ''' A split in a transaction '''
    def __init__(self):
        self.account = None
        self.amount = None
        self.currency = None


class LedgerTransaction():
    ''' represents a ledger transaction '''
    def __init__(self):
        self.date = None
        self.payee = ""
        self.comment = ""
        self.postings = []

class LedgerExporter():
    from moneymanagerexlib.dal import CheckingAccount, SplitTransaction
    from typing import List

    def __init__(self, mmex_db_path):
        self.mmex_db_path = mmex_db_path

    def export(self):
        '''
        For now, export everything. Add filtering later.
        Print result to STDOUT, allowing redirection to an output file.
        '''
        from moneymanagerexlib.query import QueryFilter, Queries
        
        # load all transactions and splits, order by date (happens automatically).
        q = Queries(self.mmex_db_path)
        txs = q.load_transactions(None)

        output = ""

        for tx in txs:
            splits = q.load_splits_for(tx)
            # export to ledger format
            ledger_tx = self._LedgerExporter__get_ledger_from_mmex(tx, splits)
            line = self._LedgerExporter__get_txt_from_ledger(ledger_tx)
            output += line

        return output

    def __get_ledger_from_mmex(self, tx: CheckingAccount, splits: List[SplitTransaction]):
        ''' write ledger transactions '''
        ltx = LedgerTransaction()
        ltx.date = tx.TRANSDATE
        if tx.payee:
            ltx.payee = tx.payee.PAYEENAME
        ltx.comment = tx.NOTES

        # main posting
        p1 = LedgerPosting()
        p1.account = f"{tx.account.HELDAT}:{tx.account.ACCOUNTNAME}"
        amount = tx.TRANSAMOUNT
        if tx.TRANSCODE == 'Withdrawal':
            amount = amount * (-1)
        p1.amount = amount
        p1.currency = tx.account.currency.CURRENCY_SYMBOL
        ltx.postings.append(p1)

        # expense?
        if tx.CATEGID != -1:
            # expense
            # A simple 2-sided transaction
            p2 = LedgerPosting()
            category = tx.category.CATEGNAME
            # We'll make an assumption that, if it's not Income, it is an Expense.
            if not category.startswith('Income'):
                category = "Expenses:" + tx.category.CATEGNAME
            p2.account = category
            if tx.subcategory:
                p2.account += f":{tx.subcategory.SUBCATEGNAME}"
            p2.amount = ""
            p2.currency = ""
            ltx.postings.append(p2)
        
        # multiple splits?
        if splits:
            # Multiple splits. Add postings.
            for split in splits:
                p = LedgerPosting()
                p.account = split.category.CATEGNAME
                if split.subcategory:
                    p.account += f":{split.subcategory.SUBCATEGNAME}"
                p.amount = split.SPLITTRANSAMOUNT
                if p1.amount > 0:
                    p.amount = p.amount * (-1)
                p.currency = tx.account.currency.CURRENCY_SYMBOL
                ltx.postings.append(p)

        # transfer?
        # todo: handle multi-currency transfer. Insert trading account records?
        if tx.TOACCOUNTID != -1:
            # Transfer
            p1.amount *= -1 # The first is always the "From" account.
            
            p2 = LedgerPosting()
            p2.account = f"{tx.account_to.HELDAT}:{tx.account_to.ACCOUNTNAME}"
            p2.amount = tx.TOTRANSAMOUNT
            p2.currency = tx.account_to.currency.CURRENCY_SYMBOL
            ltx.postings.append(p2)

        return ltx

    def __get_txt_from_ledger(self, tx: LedgerTransaction):
        ''' print ledger objects to text '''
        output = f"{tx.date} {tx.payee}\n"
        
        if tx.comment:
            output += f"\t; {tx.comment}\n"
        
        for p in tx.postings:
            output += f"\t{p.account}    {p.amount} {p.currency}\n"

        output += "\n"
        return output
