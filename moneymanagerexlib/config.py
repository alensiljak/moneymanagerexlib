'''
Configuration object to be used by the caller.
'''

class Configuration:
    ''' Send an instance of this to the main method of the library '''
    db_path = ''
    accounts = []
